#!/bin/bash

docker-compose up -d --scale kafka=3 --scale spark-worker=3 --scale datanode=3 --build