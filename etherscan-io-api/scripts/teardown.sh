#!/bin/bash

docker stop $(docker ps --filter "name=etherscan-io-api" -q)
docker rm -f $(docker ps --filter "name=etherscan-io-api" -aq)
docker rmi etherscan-io-api:1.0