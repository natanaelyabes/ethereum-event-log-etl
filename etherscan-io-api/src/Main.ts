// Library
import * as ts from '@netanyahuyasser/file-system-service';
import { AxiosResponse } from 'axios';
import dotenv from 'dotenv';
import fs from 'fs';
import path from 'path';
import { TSMap } from 'typescript-map';
import web3 from 'web3';

// Classes
import { AccountApi } from './etherscan-io/api/AccountApi';
import { BlockApi } from './etherscan-io/api/BlockApi';
import { ContractApi } from './etherscan-io/api/ContractApi';
import { EventLogApi } from './etherscan-io/api/EventLogApi';
import { GethApi } from './etherscan-io/api/GethApi';
import { StatApi } from './etherscan-io/api/StatApi';
import { TokenApi } from './etherscan-io/api/TokenApi';
import { TransactionApi } from './etherscan-io/api/TransactionApi';

// Interfaces
import { IContractABI } from './etherscan-io/api/interfaces/models/contracts/IContractABI';
import { IContractSourceCode } from './etherscan-io/api/interfaces/models/contracts/IContractSourceCodes';
import { IEvent, IEventLog, IEventLogs } from './etherscan-io/api/interfaces/models/logs/IEventLogs';

// Shorthands
import FileSystemService = ts.kr.ac.pusan.bsclab.FileSystemService;
import { IXLog } from './etherscan-io/IXLog';

// dotenv
dotenv.config();

/**
 *
 * @author Natanael Yabes Wirawan <yabes.wirawan@gmail.com>
 * @export
 * @class Main
 */
export default class Main {

  // API Configurations
  private static BASE_URL = process.env.ETHERSCAN_BASE_URL as string;
  private static API_TOKEN = process.env.ETHERSCAN_API_TOKEN as string;

  // APIs
  private static account: AccountApi;
  private static block: BlockApi;
  private static contract: ContractApi;
  private static log: EventLogApi;
  private static geth: GethApi;
  private static stat: StatApi;
  private static token: TokenApi;
  private static transaction: TransactionApi;

  // Contract information
  private static CONTRACT_ADDRESS = '0x06012c8cf97BEaD5deAe237070F9587f8E7A266d'; // CryptoKitties Core
  private static CONTRACT_ABI: IContractABI;
  private static CONTRACT_SOURCE_CODE: IContractSourceCode;

  // Web3 instance
  private static web3 = new web3(new web3.providers.HttpProvider(process.env.WEB3_PROVIDER_URL as string));

  // ABI decoder
  private static abiDecoder = require('abi-decoder');

  // Ethereum input data decoder
  private static InputDataDecoder = require('ethereum-input-data-decoder');
  private static decoder: any;

  // BN.js
  private static BN = require('bn.js');

  /**
   * To run everything
   *
   * @returns {Promise<void>}
   * @memberof Test
   */
  public static async run(): Promise<void> {

    // Initialize API
    await this.initApi();

    // Get contract information
    await this.getContractInformation();

    // Initialize decoder
    await this.initDecoder();

    // // Genesis
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4605167, 4605566, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 3000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4605567, 4605866, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 6000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4605867, 4606166, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 9000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4606167, 4606366, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 12000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4606367, 4606666, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 15000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4606667, 4606966, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 18000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4606967, 4607266, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 21000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(4607267, 4608187, path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core')); }, 24000);

    // // Everyday
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6605100, 6605999, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 27000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6606000, 6606300, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 30000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6606301, 6606900, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 33000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6607300, 6607599, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 36000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6607600, 6607899, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 39000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6607900, 6608499, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 42000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6608500, 6608999, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 45000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6609000, 6609699, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 48000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6609700, 6610399, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 51000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6609700, 6610399, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 54000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6610400, 6611199, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 57000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6611200, 6611999, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 60000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6612000, 6612799, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 63000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6612800, 6613299, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 66000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6613300, 6613599, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 69000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6613600, 6613899, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 72000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6613900, 6614399, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 75000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6614400, 6615099, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 78000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6615100, 6615799, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 81000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6615800, 6616599, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 84000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6616600, 6617399, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 87000);
    // setTimeout(async () => { await this.getCryptoKittiesEventLog(6617400, 6618100, path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core')); }, 90000);

    // // Merge log
    // setTimeout(async () => { await this.mergeLog(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'core'), path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 'all.json'); }, 93000);
    // setTimeout(async () => { await this.mergeLog(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'core'), path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 'all.json'); }, 96000);

    // const CRYPTOKITTIES_CORE = '0x06012c8cf97BEaD5deAe237070F9587f8E7A266d';
    // const CRYPTOKITTIES_SALES_AUCTION = '0xb1690c08e213a35ed9bab7b318de14420fb57d8c';
    // const CRYPTOKITTIES_SIRING_AUCTION = '0xc7af99fe5513eb6710e6d5f44f9989da40f27f26';

    // // Get additional core events
    // this.CONTRACT_ADDRESS = CRYPTOKITTIES_CORE;
    // this.getContractInformation();
    // this.initDecoder();

    // // Genesis
    // const e1 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4605167, 4605566);
    // const e2 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4605567, 4605866);
    // const e3 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4605867, 4606166);
    // const e4 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606167, 4606366);
    // const e5 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606367, 4606666);
    // const e6 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606667, 4606966);
    // const e7 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606967, 4607266);
    // const e8 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4607267, 4608187);

    // // Get additional sales auction events
    // this.CONTRACT_ADDRESS = CRYPTOKITTIES_SALES_AUCTION;
    // this.getContractInformation();
    // this.initDecoder();

    // // Genesis
    // const e9 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4605167, 4605566);
    // const e10 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4605567, 4605866);
    // const e11 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4605867, 4606166);
    // const e12 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606167, 4606366);
    // const e13 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606367, 4606666);
    // const e14 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606667, 4606966);
    // const e15 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4606967, 4607266);
    // const e16 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 4607267, 4608187);
    // await this.saveAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 'all.json', [e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16]);

    // // Get additional core events
    // this.CONTRACT_ADDRESS = CRYPTOKITTIES_CORE;
    // this.getContractInformation();
    // this.initDecoder();

    // // Everyday
    // const e17 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6605100, 6605999);
    // const e18 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6606000, 6606300);
    // const e19 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6606301, 6606900);
    // const e20 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6607300, 6607599);
    // const e21 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6607600, 6607899);
    // const e22 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6607900, 6608499);
    // const e23 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6608500, 6608999);
    // const e24 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6609000, 6609699);
    // const e25 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6609700, 6610399);
    // const e26 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6609700, 6610399);
    // const e27 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6610400, 6611199);
    // const e28 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6611200, 6611999);
    // const e29 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6612000, 6612799);
    // const e30 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6612800, 6613299);
    // const e31 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6613300, 6613599);
    // const e32 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6613600, 6613899);
    // const e33 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6613900, 6614399);
    // const e34 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6614400, 6615099);
    // const e35 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6615100, 6615799);
    // const e36 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6615800, 6616599);
    // const e37 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6616600, 6617399);
    // const e38 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6617400, 6618100);

    // // Get additional sales auction events
    // this.CONTRACT_ADDRESS = CRYPTOKITTIES_SALES_AUCTION;
    // this.getContractInformation();
    // this.initDecoder();

    // // Everyday
    // const e39 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6605100, 6605999);
    // const e40 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6606000, 6606300);
    // const e41 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6606301, 6606900);
    // const e42 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6607300, 6607599);
    // const e43 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6607600, 6607899);
    // const e44 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6607900, 6608499);
    // const e45 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6608500, 6608999);
    // const e46 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6609000, 6609699);
    // const e47 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6609700, 6610399);
    // const e48 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6609700, 6610399);
    // const e49 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6610400, 6611199);
    // const e50 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6611200, 6611999);
    // const e51 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6612000, 6612799);
    // const e52 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6612800, 6613299);
    // const e53 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6613300, 6613599);
    // const e54 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6613600, 6613899);
    // const e55 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6613900, 6614399);
    // const e56 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6614400, 6615099);
    // const e57 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6615100, 6615799);
    // const e58 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6615800, 6616599);
    // const e59 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6616600, 6617399);
    // const e60 = await this.getAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 6617400, 6618100);
    // await this.saveAdditionalEvent(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 'all.json', [e17, e18, e19, e20, e21, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39, e40, e41, e42, e43, e44, e45, e46, e47, e48, e49, e50, e51, e52, e53, e54, e55, e56, e57, e58, e59, e60]);

    // this.saveAsCSV(path.join('public', 'json', 'ethereum', 'cryptokitties', 'genesis'), 'complete.json');
    // this.saveAsCSV(path.join('public', 'json', 'ethereum', 'cryptokitties', 'everyday'), 'complete.json');

    this.uploadToHDFS();
  }

  /**
   * To initialize decoder
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async initDecoder(): Promise<void> {
    this.abiDecoder.addABI(JSON.parse(this.CONTRACT_ABI.result));
    this.decoder = new this.InputDataDecoder(JSON.parse(this.CONTRACT_ABI.result));
  }

  /**
   * To initialize API
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async initApi(): Promise<void> {
    this.account = new AccountApi(this.BASE_URL, this.API_TOKEN);
    this.block = new BlockApi(this.BASE_URL, this.API_TOKEN);
    this.contract = new ContractApi(this.BASE_URL, this.API_TOKEN);
    this.log = new EventLogApi(this.BASE_URL, this.API_TOKEN);
    this.geth = new GethApi(this.BASE_URL, this.API_TOKEN);
    this.stat = new StatApi(this.BASE_URL, this.API_TOKEN);
    this.token = new TokenApi(this.BASE_URL, this.API_TOKEN);
    this.transaction = new TransactionApi(this.BASE_URL, this.API_TOKEN);
  }

  /**
   * Get contract information
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async getContractInformation(): Promise<void> {
    await this.getContractABI();
    await this.getContractSourceCode();
    await this.saveContractABI();
  }

  /**
   * Save contract ABI as JSON file
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async saveContractABI(): Promise<void> {
    const ABI_JSON = JSON.parse(this.CONTRACT_ABI.result);

    const PATH = path.join(process.cwd(), 'public', 'json');
    const FILE_NAME = 'abi.json';

    if (!fs.existsSync(path.join(PATH, FILE_NAME))) {
      FileSystemService.saveData(PATH, FILE_NAME, ABI_JSON);
    }
  }

  /**
   * Fetch the contract ABI
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async getContractABI(): Promise<void> {
    const contractAddress = this.CONTRACT_ADDRESS;
    const contractABI = await this.contract.getContractABI(contractAddress);
    this.CONTRACT_ABI = contractABI.data;
  }

  /**
   * Fetch the contract source code
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async getContractSourceCode(): Promise<void> {
    const contractAddress = this.CONTRACT_ADDRESS;
    const contractSourceCode = await this.contract.getContractSourceCode(contractAddress);
    this.CONTRACT_SOURCE_CODE = contractSourceCode.data;
  }

  /**
   * Fetch CryptoKitties event logs
   *
   * @private
   * @static
   * @returns {Promise<void>}
   * @memberof Main
   */
  private static async getCryptoKittiesEventLog(from: number, to: number, dir: string): Promise<void> {

    const CRYPTOKITTIES_CORE = '0x06012c8cf97BEaD5deAe237070F9587f8E7A266d';

    this.CONTRACT_ADDRESS = CRYPTOKITTIES_CORE;

    // Construct query parameter
    const contractAddress = this.CONTRACT_ADDRESS;
    const fromBlock = from;
    const toBlock = to;

    // Fetch event logs
    const logs: AxiosResponse<IEventLogs> = await this.log.getLogs(contractAddress, fromBlock, toBlock);

    console.log(logs.data.result.length);

    // Decoded logs
    const decodedLogs = this.abiDecoder.decodeLogs(logs.data.result);

    // XLogs
    const XLogs: TSMap<string, IXLog[]> = new TSMap<string, IXLog[]>();

    // For loop
    for (let i = 0; i < logs.data.result.length; i++) {

      // Reassign data field to decoded logs
      logs.data.result[i].data = decodedLogs[i] as IEvent;

      // Collect the initial activity of CryptoKitties
      if ((logs.data.result[i].data as IEvent).name === 'Birth') {
        XLogs.set((logs.data.result[i].data as IEvent).events[1].value, [{
          activity: (logs.data.result[i].data as IEvent).name.toLowerCase(),
          blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
          caseId: (logs.data.result[i].data as IEvent).events[1].value,
          eventId: logs.data.result[i].transactionHash,
          timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
        }]);
      }
    }

    const dirPath = path.join(process.cwd(), dir);

    FileSystemService.saveData(dirPath, `${from}.json`, XLogs);
  }

  private static async mergeLog(dir: string, dest: string, filename: string): Promise<void> {
    const dirPath = path.join(process.cwd(), dir);
    const files = fs.readdirSync(dirPath);

    const kitties: TSMap<string, IXLog> = new TSMap<string, IXLog>();

    for (const file of files) {
      const data = await FileSystemService.loadData(path.join(dirPath, file));
      const parsed = JSON.parse(data);
      const keys = Object.keys(parsed);

      for (const key of keys) {
        kitties.set(key, parsed[key]);
      }
    }

    FileSystemService.saveData(dest, filename, kitties);
  }

  private static async getAdditionalEvent(dir: string, from: number, to: number): Promise<IXLog[]> {
    const dirPath = path.join(process.cwd(), dir);
    let data = await FileSystemService.loadData(path.join(dirPath, 'all.json'));
    data = JSON.parse(data);

    const keys = Object.keys(data);

    const kitties: Map<string, IXLog[]> = new Map<string, IXLog[]>();

    for (const key of keys) {
      kitties.set(key, data[key]);
    }

    // Construct query parameter
    const contractAddress = this.CONTRACT_ADDRESS;
    const fromBlock =  from;
    const toBlock = to; // 6618100

    // Fetch event logs
    const logs: AxiosResponse<IEventLogs> = await this.log.getLogs(contractAddress, fromBlock, toBlock);

    // Decoded logs
    const decodedLogs = this.abiDecoder.decodeLogs(logs.data.result);

    const events: IXLog[] = [];

    // For each log data
    for (let i = 0; i < logs.data.result.length; i++) {

      console.log(from, to, decodedLogs[i]);

      // Reassign data field to decoded logs
      logs.data.result[i].data = decodedLogs[i] as IEvent;

      // Exclude birth event
      if ((logs.data.result[i].data as IEvent).name !== 'Birth') {

        // To handle Transfer event
        if ((logs.data.result[i].data as IEvent).name === 'Transfer') {
          const event = logs.data.result[i].data as IEvent;
          const key = event.events[2].value;

          events.push({
            activity: (logs.data.result[i].data as IEvent).name.toLowerCase(),
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: (logs.data.result[i].data as IEvent).events[2].value,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }

        if ((logs.data.result[i].data as IEvent).name === 'Pregnant') {
          const event = logs.data.result[i].data as IEvent;

          const matronId = event.events[1].value;
          const sireId = event.events[2].value;

          events.push({
            activity: (logs.data.result[i].data as IEvent).name.toLowerCase(),
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: matronId,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });

          events.push({
            activity: 'sire made someone pregnant',
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: sireId,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }

        if ((logs.data.result[i].data as IEvent).name === 'AuctionCreated') {
          const event = logs.data.result[i].data as IEvent;
          const key = event.events[0].value;

          events.push({
            activity: 'auction created',
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: key,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }

        if ((logs.data.result[i].data as IEvent).name === 'AuctionSuccessful') {
          const event = logs.data.result[i].data as IEvent;
          const key = event.events[0].value;

          events.push({
            activity: 'auction successful',
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: key,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }

        if ((logs.data.result[i].data as IEvent).name === 'AuctionCancelled') {
          const event = logs.data.result[i].data as IEvent;
          const key = event.events[0].value;

          events.push({
            activity: 'auction cancelled',
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: key,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }
      }

      if ((logs.data.result[i].data as IEvent).name === 'Birth') {
        const event = logs.data.result[i].data as IEvent;
        const key = event.events[2].value;

        const matronId = event.events[2].value;
        const sireId = event.events[3].value;

        if (key === matronId) {
          events.push({
            activity: 'kitty became mother',
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: (logs.data.result[i].data as IEvent).events[2].value,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }

        if (key === sireId) {
          events.push({
            activity: 'kitty became father',
            blockNumber: this.web3.utils.hexToNumberString(logs.data.result[i].blockNumber),
            caseId: (logs.data.result[i].data as IEvent).events[2].value,
            eventId: logs.data.result[i].transactionHash,
            timestamp: this.web3.utils.hexToNumberString(logs.data.result[i].timeStamp),
          });
        }
      }
    }

    return events;
  }

  private static async saveAdditionalEvent(dir: string, filename: string, additionalEvents: IXLog[][]): Promise<void> {
    const data = await FileSystemService.loadData(path.join(dir, filename));

    const parsed = JSON.parse(data);
    const keys = Object.keys(parsed);

    const kitties: TSMap<string, IXLog[]> = new TSMap<string, IXLog[]>();

    for (const key of keys) {
      kitties.set(key, parsed[key]);
    }

    for (const events of additionalEvents) {
      for (const event of events) {
        if (kitties.get(event.caseId) !== undefined) {
          (kitties.get(event.caseId) as IXLog[]).push(event);
        }
      }
    }

    // console.log(kitties.get('0'));
    FileSystemService.saveData(dir, 'complete.json', kitties);
  }

  private static async saveAsCSV(dir: string, filename: string) {
    let data = await FileSystemService.loadData(path.join(dir, filename));
    data = JSON.parse(data);
    const keys = Object.keys(data);

    const kitties: Map<string, IXLog[]> = new Map<string, IXLog[]>();

    // Header
    const rows = [['case', 'event', 'activity', 'timestamp', 'blockNumber']];

    for (const key of keys) {
      kitties.set(key, data[key]);
      const events = (kitties.get(key) as IXLog[]);
      for (const event of events) {
        const time = new Date(parseInt(event.timestamp, 10) * 1000);

        const year = time.getFullYear();
        const month = (time.getMonth() + 1) < 10 ? `0${(time.getMonth() + 1)}` : (time.getMonth() + 1);
        const date = time.getDate() < 10 ? `0${time.getDate()}` : time.getDate();

        const hour = time.getHours() < 10 ? `0${time.getHours()}` : time.getHours();
        const minute = time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes();
        const seconds = time.getSeconds() < 10 ? `0${time.getSeconds()}` : time.getSeconds();

        const caseId = event.caseId;
        const eventId = event.eventId;
        const activity = event.activity;
        // const timestamp = `${year}-${month}-${date} ${hour}:${minute}:${seconds}`;
        const timestamp = parseInt(event.timestamp, 10) * 1000;
        const blockNumber = event.blockNumber;
        rows.push([caseId, eventId, activity, timestamp.toString(), blockNumber]);
      }
    }

    const fastcsv = require('fast-csv');
    const ws = fs.createWriteStream(path.join(dir, 'complete.csv'));
    fastcsv.write(rows, { headers: true }).pipe(ws);
  }

  private static async uploadToHDFS(): Promise<void> {
    const WebHDFS = require('webhdfs');
    const hdfs = WebHDFS.createClient({
      host: '172.18.0.8',
      path: '/webhdfs/v1',
      port: 9870,
      user: 'root',
    });

    const genesisCSV = fs.createReadStream(path.join(process.cwd(), 'public', 'json', 'ethereum', 'cryptokitties', 'genesis', 'complete.csv'));
    const everydayCSV = fs.createReadStream(path.join(process.cwd(), 'public', 'json', 'ethereum', 'cryptokitties', 'everyday', 'complete.csv'));

    const genesisRemote = hdfs.createWriteStream('/json/ethereum/cryptokitties/genesis/complete.csv');
    const everydayRemote = hdfs.createWriteStream('/json/ethereum/cryptokitties/everyday/complete.csv');

    genesisCSV.pipe(genesisRemote);
    everydayCSV.pipe(everydayRemote);

    genesisRemote.on('error', (err: any) => {
      console.log(err);
    });

    everydayRemote.on('finish', () => {
      console.log('Finish uploading');
    });
  }
}

Main.run();
