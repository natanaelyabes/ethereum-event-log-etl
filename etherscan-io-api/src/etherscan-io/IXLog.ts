export interface IXLog {
  caseId: string;
  eventId: string;
  timestamp: string;
  blockNumber: string;
  activity: string;
}
