import { IEtherscanApi } from './interfaces/IEtherscanApi';

import axios from 'axios';

/**
 *
 *
 * @export
 * @class EtherscanApi
 * @implements {IEtherscanApi}
 */
export class EtherscanApi implements IEtherscanApi {
  private _BASE_URL?: string;
  private _API_TOKEN?: string;
  private _module?: string;
  private _action?: string;

  constructor(BASE_URL: string, API_TOKEN: string) {
    this._BASE_URL = BASE_URL;
    this._API_TOKEN = API_TOKEN;
  }

  public get BASE_URL(): string {
    return this._BASE_URL as string;
  }

  public set BASE_URL(BASE_URL: string) {
    this._BASE_URL = BASE_URL;
  }

  public get API_TOKEN(): string {
    return this._API_TOKEN as string;
  }

  public set API_TOKEN(API_TOKEN: string) {
    this._API_TOKEN = API_TOKEN;
  }

  public get module(): string {
    return this._module as string;
  }

  public set module(module: string) {
    this._module = module;
  }

  public get action(): string {
    return this._action as string;
  }

  public set action(action: string) {
    this._action = action;
  }

  public async fetch(url: string): Promise<any> {
    const data = await axios.get(url);
    return data;
  }
}
