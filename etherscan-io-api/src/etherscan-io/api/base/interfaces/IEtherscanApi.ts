/**
 *
 *
 * @export
 * @interface IEtherscanApi
 */
export interface IEtherscanApi {
  BASE_URL: string;
  API_TOKEN: string;
  module: string;
  action: string;
}
