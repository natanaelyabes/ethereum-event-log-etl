export const MODULE_NAME = 'stats';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  ethsupply = 'ethsupply',
  ethprice = 'ethprice',
  chainsize = 'chainsize',
}
