export const MODULE_NAME = 'logs';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  getlogs = 'getLogs',
}
