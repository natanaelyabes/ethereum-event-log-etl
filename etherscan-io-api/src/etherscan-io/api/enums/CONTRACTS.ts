export const MODULE_NAME = 'contract';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  getabi = 'getabi',
  getsourcecode = 'getsourcecode',
}
