export const MODULE_NAME = 'proxy';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  eth_blockNumber = 'eth_blockNumber',
  eth_getBlockByNumber = 'eth_getBlockByNumber',
  eth_getUncleByNumberAndIndex = 'eth_getUncleByNumberAndIndex',
  eth_getBlockTransactionCountByNumber = 'eth_getBlockTransactionCountByNumber',
  eth_getTransactionByHash = 'eth_getTransactionByHash',
  eth_getTransactionByBlockNumberAndIndex = 'eth_getTransactionByBlockNumberAndIndex',
  eth_getTransactionCount = 'eth_getTransactionCount',
  eth_sendRawTransaction = 'eth_sendRawTransaction',
  eth_getTransactionReceipt = 'eth_getTransactionReceipt',
  eth_call = 'eth_call',
  eth_getCode = 'eth_getCode',
  eth_getStorageAt = 'eth_getStorageAt',
  eth_gasPrice = 'eth_gasPrice',
  eth_estimateGas = 'eth_estimateGas',
}
