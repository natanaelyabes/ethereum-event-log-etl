export const MODULE_NAME = 'transaction';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  getstatus = 'getstatus',
  gettxreceiptstatus = 'gettxreceiptstatus',
}
