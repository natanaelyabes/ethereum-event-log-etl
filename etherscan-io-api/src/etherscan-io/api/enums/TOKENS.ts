export const MODULE_NAME = 'stats';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  tokensupply = 'tokensupply',
  tokenbalance = 'tokenbalance',
}
