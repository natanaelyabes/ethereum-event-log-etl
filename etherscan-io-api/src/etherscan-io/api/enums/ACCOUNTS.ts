export const MODULE_NAME = 'account';

/**
 *
 *
 * @export
 * @enum {number}
 */
export enum ACTIONS {
  balance = 'balance',
  balancemulti = 'balancemulti',
  txlist = 'txlist',
  txlistinternal = 'txlistinternal',
  tokentx = 'tokentx',
  getminedblocks = 'getminedblocks',
}
