import { MODULE_NAME, ACTIONS } from './enums/CONTRACTS';
import { IContractApi } from './interfaces/IContractApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { IContractABI } from './interfaces/models/contracts/IContractABI';
import { IContractSourceCode } from './interfaces/models/contracts/IContractSourceCodes';

/**
 *
 *
 * @export
 * @class ContractApi
 * @extends {EtherscanApi}
 * @implements {IContractApi}
 */
export class ContractApi extends EtherscanApi implements IContractApi {

  /**
   * Creates an instance of ContractApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof ContractApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @param {string} address
   * @returns {Promise<AxiosResponse<IContractABI>>}
   * @memberof ContractApi
   */
  public async getContractABI(address: string): Promise<AxiosResponse<IContractABI>> {

    // Set action
    this.action = ACTIONS.getabi;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddress].join('&');

    // Construct url
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch contractABI
    const contractABI: AxiosResponse<IContractABI> = await this.fetch(url);

    // Return contractABI
    return contractABI;
  }

  /**
   *
   *
   * @param {string} address
   * @returns {Promise<AxiosResponse<IContractSourceCode>>}
   * @memberof ContractApi
   */
  public async getContractSourceCode(address: string): Promise<AxiosResponse<IContractSourceCode>> {

    // Set action
    this.action = ACTIONS.getsourcecode;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddress].join('&');

    // Construct url
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch contractABI
    const contractSourceCode: AxiosResponse<IContractSourceCode> = await this.fetch(url);

    // Return contractABI
    return contractSourceCode;
  }
}
