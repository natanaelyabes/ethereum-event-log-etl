import { MODULE_NAME, ACTIONS } from './enums/BLOCKS';
import { IBlockApi } from './interfaces/IBlockApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { IBlockReward } from './interfaces/models/blocks/IBlockReward';

/**
 *
 *
 * @export
 * @class BlockApi
 * @extends {EtherscanApi}
 * @implements {IBlockApi}
 */
export class BlockApi extends EtherscanApi implements IBlockApi {

  /**
   * Creates an instance of BlockApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof BlockApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @param {number} blockNo
   * @returns {Promise<AxiosResponse<IBlockReward>>}
   * @memberof BlockApi
   */
  public async getBlockReward(blockNo: number): Promise<AxiosResponse<IBlockReward>> {

    // Set action
    this.action = ACTIONS.getblockreward;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pBlockNo = `blockno=${blockNo}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pBlockNo].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch blockReward
    const blockReward: AxiosResponse<IBlockReward> = await this.fetch(url);

    return blockReward;
  }
}
