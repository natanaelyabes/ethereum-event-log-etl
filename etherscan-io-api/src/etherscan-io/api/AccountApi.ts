import { MODULE_NAME, ACTIONS } from './enums/ACCOUNTS';
import { EtherscanApi } from './base/EtherscanApi';
import { IAccountApi, ILimit } from './interfaces/IAccountApi';
import { IEtherBalance } from './interfaces/models/accounts/IEtherBalance';
import { IEtherBalanceMultiAddress } from './interfaces/models/accounts/IEtherBalanceMultiAddress';
import { INormalTransactions } from './interfaces/models/accounts/INormalTransactions';
import { IInternalTransactions } from './interfaces/models/accounts/IInternalTransactions';
import { IERC20TokenTransferEventTransactions } from './interfaces/models/accounts/IERC20TokenTransferEventTransactions';
import { IMinedBlocks } from './interfaces/models/accounts/IMinedBlocks';

import axios, { AxiosResponse } from 'axios';

/**
 *
 *
 * @export
 * @class AccountApi
 * @extends {EtherscanApi}
 * @implements {IAccountApi}
 */
export class AccountApi extends EtherscanApi implements IAccountApi {

  /**
   * Creates an instance of AccountApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof AccountApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @param {string[]} addresses
   * @param {string} tag
   * @returns {(Promise<AxiosResponse<IEtherBalance | IEtherBalanceMultiAddress>>)}
   * @memberof AccountApi
   */
  public async getEtherBalance(addresses: string[], tag: string): Promise<AxiosResponse<IEtherBalance | IEtherBalanceMultiAddress>> {

    // Check if multi address is supplied
    const isMultiAddress: boolean = addresses.length > 1 ? true : false;

    // Set action
    this.action = isMultiAddress ? ACTIONS.balancemulti : ACTIONS.balance;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddresses = `address=${addresses.join(',')}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddresses, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch balance
    const balance: AxiosResponse<IEtherBalance | IEtherBalanceMultiAddress> = await this.fetch(url);

    // Return balance
    return balance;
  }

  /**
   *
   *
   * @param {string} address
   * @param {number} startBlock
   * @param {number} endBlock
   * @param {('asc' | 'desc')} sort
   * @param {(ILimit | undefined)} [limit]
   * @returns {Promise<AxiosResponse<INormalTransactions>>}
   * @memberof AccountApi
   */
  public async getNormalTransactionsByAddress(address: string, startBlock: number, endBlock: number, sort: 'asc' | 'desc', limit?: ILimit | undefined): Promise<AxiosResponse<INormalTransactions>> {

    // Set action
    this.action = ACTIONS.txlist;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pStartBlock = `startblock=${startBlock}`;
    const pEndBlock = `endblock=${endBlock}`;
    const pSort = `sort=${sort}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;

    // Set pagination
    const pLimit: string = limit ? `page=${limit.page}&offset=${limit.offset}` : '';

    // Set parameter
    const p = limit ?
      [pModule, pAction, pAddress, pStartBlock, pEndBlock, pSort, pLimit].join('&') :
      [pModule, pAction, pAddress, pStartBlock, pEndBlock, pSort].join('&')
    ;

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch normalTxs
    const normalTxs: AxiosResponse<INormalTransactions> = await this.fetch(url);

    // Return normalTxs
    return normalTxs;
  }

  /**
   *
   *
   * @param {string} address
   * @param {number} startBlock
   * @param {number} endBlock
   * @param {('asc' | 'desc')} sort
   * @param {(ILimit | undefined)} [limit]
   * @returns {Promise<AxiosResponse<IInternalTransactions>>}
   * @memberof AccountApi
   */
  public async getInternalTransactionsByAddress(address: string, startBlock: number, endBlock: number, sort: 'asc' | 'desc', limit?: ILimit | undefined): Promise<AxiosResponse<IInternalTransactions>> {

    // Set action
    this.action = ACTIONS.txlistinternal;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pStartBlock = `startblock=${startBlock}`;
    const pEndBlock = `endblock=${endBlock}`;
    const pSort = `sort=${sort}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;

    // Set pagination
    const pLimit: string = limit ? `page=${limit.page}&offset=${limit.offset}` : '';

    // Set parameter
    const p = limit ?
      [pModule, pAction, pAddress, pStartBlock, pEndBlock, pSort, pLimit].join('&') :
      [pModule, pAction, pAddress, pStartBlock, pEndBlock, pSort].join('&')
    ;

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch internalTxs
    const internalTxs: AxiosResponse<IInternalTransactions> = await this.fetch(url);

    // Return internalTxs
    return internalTxs;
  }

  /**
   *
   *
   * @param {string} txHash
   * @returns {Promise<AxiosResponse<IInternalTransactions>>}
   * @memberof AccountApi
   */
  public async getInternalTransactionsByHash(txHash: string): Promise<AxiosResponse<IInternalTransactions>> {

    // Set action
    this.action = ACTIONS.txlistinternal;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTxHash = `txhash=${txHash}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;

    // Set parameter
    const p = [pModule, pAction, pTxHash].join('&');

    // Construct URL
    const url: string = `${this.BASE_URL}?${p}&apikey=${this.API_TOKEN}`;

    // Fetch internalTxs
    const internalTxs: AxiosResponse<IInternalTransactions> = await this.fetch(url);

    // Return internalTxs
    return internalTxs;
  }

  /**
   *
   *
   * @param {string} address
   * @param {number} startBlock
   * @param {number} endBlock
   * @param {('asc' | 'desc')} sort
   * @param {(ILimit | undefined)} [limit]
   * @returns {Promise<AxiosResponse<IERC20TokenTransferEventTransactions>>}
   * @memberof AccountApi
   */
  public async getERC20TokenTransferEventTransactionByAddress(address: string, startBlock: number, endBlock: number, sort: 'asc' | 'desc', limit?: ILimit | undefined): Promise<AxiosResponse<IERC20TokenTransferEventTransactions>> {

    // Set action
    this.action = ACTIONS.tokentx;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pStartBlock = `startblock=${startBlock}`;
    const pEndBlock = `endblock=${endBlock}`;
    const pSort = `sort=${sort}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;

    // Set pagination
    const pLimit: string = limit ? `page=${limit.page}&offset=${limit.offset}` : '';

    // Set parameter
    const p = limit ?
      [pModule, pAction, pAddress, pStartBlock, pEndBlock, pSort, pLimit].join('&') :
      [pModule, pAction, pAddress, pStartBlock, pEndBlock, pSort].join('&')
    ;

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ERC20TokenTransferEventTransactions
    const ERC20TokenTransferEventTransactions: AxiosResponse<IERC20TokenTransferEventTransactions> = await this.fetch(url);

    // Return ERC20TokenTransferEventTransactions
    return ERC20TokenTransferEventTransactions;
  }

  /**
   *
   *
   * @param {string} address
   * @param {('blocks' | 'uncles')} blockType
   * @param {(ILimit | undefined)} [limit]
   * @returns {Promise<AxiosResponse<IMinedBlocks>>}
   * @memberof AccountApi
   */
  public async getMinedBlocks(address: string, blockType: 'blocks' | 'uncles', limit?: ILimit | undefined): Promise<AxiosResponse<IMinedBlocks>> {

    // Set action
    this.action = ACTIONS.getminedblocks;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pBlockType = `blocktype=${blockType}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;

    // Set pagination
    const pLimit: string = limit ? `page=${limit.page}&offset=${limit.offset}` : '';

    // Set parameter
    const p = limit ?
      [pModule, pAction, pAddress, pBlockType, pLimit].join('&') :
      [pModule, pAction, pAddress, pBlockType].join('&')
    ;

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch minedBlocks
    const minedBlocks: AxiosResponse<IMinedBlocks> = await this.fetch(url);

    // Return minedBlocks
    return minedBlocks;
  }
}
