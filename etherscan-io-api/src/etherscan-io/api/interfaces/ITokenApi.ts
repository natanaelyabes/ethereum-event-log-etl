import { AxiosResponse } from 'axios';
import { ISimpleResult } from './models/ISimpleResult';

/**
 *
 *
 * @export
 * @interface ITokenApi
 */
export interface ITokenApi {
  getERC20TokenTotalSupplyByContractAddress(contractAddress: string): Promise<AxiosResponse<ISimpleResult>>;
  getERC20TokenAccountBalanceForTokenContractAddress(contractAddress: string, address: string, tag: string): Promise<AxiosResponse<ISimpleResult>>;
}
