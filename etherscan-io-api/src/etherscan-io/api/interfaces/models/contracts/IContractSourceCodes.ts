import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IContractSourceCode
 */
export interface IContractSourceCode {
  SourceCode: string;
  ABI: string;
  ContractName: string;
  CompilerVersion: string;
  OptimizationUsed: string;
  Runs: string;
  ConstructorAgreement: string;
  Library: string;
  SwarmSource: string;
}

/**
 *
 *
 * @export
 * @interface IContractSourceCodes
 * @extends {IBaseModel}
 */
export interface IContractSourceCodes extends IBaseModel {
  result: IContractSourceCode[];
}
