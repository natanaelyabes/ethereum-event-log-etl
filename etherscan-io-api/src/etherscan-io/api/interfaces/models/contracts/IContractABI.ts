import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IContractABI
 * @extends {IBaseModel}
 */
export interface IContractABI extends IBaseModel {
  result: string;
}
