import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IMinedBlock
 */
export interface IMinedBlock {
  blockNumber: string;
  timeStamp: string;
  blockReward: string;
}

/**
 *
 *
 * @export
 * @interface IMinedBlocks
 * @extends {IBaseModel}
 */
export interface IMinedBlocks extends IBaseModel {
  result: IMinedBlock[];
}
