import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IEtherBalance
 * @extends {IBaseModel}
 */
export interface IEtherBalance extends IBaseModel {
  result: string;
}
