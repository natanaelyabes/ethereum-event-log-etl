import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IERC20TokenTransferEventTransaction
 */
export interface IERC20TokenTransferEventTransaction {
  blockNumber: string;
  timeStamp: string;
  hash: string;
  nonce: string;
  blockHash: string;
  from: string;
  contractAddress: string;
  to: string;
  value: string;
  tokenName: string;
  tokenSymbol: string;
  tokenDecimal: string;
  transactionIndex: string;
  gas: string;
  gasPrice: string;
  gasUsed: string;
  cumulativeGasUsed: string;
  input: string;
  confirmations: string;
}

/**
 *
 *
 * @export
 * @interface IERC20TokenTransferEventTransactions
 * @extends {IBaseModel}
 */
export interface IERC20TokenTransferEventTransactions extends IBaseModel {
  result: IERC20TokenTransferEventTransaction[];
}
