import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IEtherBalanceByAddress
 */
export interface IEtherBalanceByAddress {
  account: string;
  balance: string;
}

/**
 *
 *
 * @export
 * @interface IEtherBalanceMultiAddress
 * @extends {IBaseModel}
 */
export interface IEtherBalanceMultiAddress extends IBaseModel {
  result: IEtherBalanceByAddress[];
}
