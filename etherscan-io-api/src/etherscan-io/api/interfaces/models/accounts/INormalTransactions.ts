import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface INormalTransaction
 */
export interface INormalTransaction {
  blockNumber: string;
  timeStamp: string;
  hash: string;
  nonce: string;
  blockHash: string;
  transactionIndex: string;
  from: string;
  to: string;
  value: string;
  gas: string;
  gasPrice: string;
  isError: '0' | '1';
  txreceipt_status: string;
  input: string | IInput;
  contractAddress: string;
  cumulativeGasUsed: string;
  gasUsed: string;
  confirmations: string;
}

export interface IInput {
  method: string;
  types: string[];
  inputs: any[];
  names: string[];
}

/**
 *
 *
 * @export
 * @interface INormalTransactions
 * @extends {IBaseModel}
 */
export interface INormalTransactions extends IBaseModel {
  result: INormalTransaction[];
}
