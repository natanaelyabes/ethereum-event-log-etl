import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IInternalTransaction
 */
export interface IInternalTransaction {
  blockNumber: string;
  timeStamp: string;
  hash: string;
  from: string;
  to: string;
  value: string;
  contractAddress: string;
  input: string;
  type: string;
  gas: string;
  gasUsed: string;
  traceId: string;
  isError: '0' | '1';
  errCode: string;
}

/**
 *
 *
 * @export
 * @interface IInternalTransactions
 * @extends {IBaseModel}
 */
export interface IInternalTransactions extends IBaseModel {
  result: IInternalTransaction[];
}
