import { IBaseGethModel } from './geth/IBaseGethModel';

/**
 *
 *
 * @export
 * @interface ISimpleResult
 * @extends {IBaseGethModel}
 */
export interface ISimpleResult extends IBaseGethModel {
  return: string;
}
