import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IReward
 */
export interface IReward {
  blockNumber: string;
  timeStamp: string;
  blockMiner: string;
  blockReward: string;
  uncles: IUncleReward[];
  uncleInclusionReward: string;
}

/**
 *
 *
 * @export
 * @interface IUncleReward
 */
export interface IUncleReward {
  miner: string;
  unclePosition: string;
  blockreward: string;
}

/**
 *
 *
 * @export
 * @interface IBlockReward
 * @extends {IBaseModel}
 */
export interface IBlockReward extends IBaseModel {
  result: IReward;
}
