import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IExecutionStatus
 */
export interface IExecutionStatus {
  isError: '0' | '1';
  errDescription: string;
}

/**
 *
 *
 * @export
 * @interface IContractExecutionStatus
 * @extends {IBaseModel}
 */
export interface IContractExecutionStatus extends IBaseModel {
  result: IExecutionStatus;
}
