import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface ITransactionReceiptStatus
 * @extends {IBaseModel}
 */
export interface ITransactionReceiptStatus extends IBaseModel {
  result: { status: '0' | '1' };
}
