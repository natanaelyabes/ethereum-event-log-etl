import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IEthereumNode
 */
export interface IEthereumNode {
  blockNumber: string;
  chainTimeStamp: string;
  chainSize: string;
  clientType: string;
  syncMode: string;
}

/**
 *
 *
 * @export
 * @interface IEthereumNodes
 * @extends {IBaseModel}
 */
export interface IEthereumNodes extends IBaseModel {
  result: IEthereumNode[];
}
