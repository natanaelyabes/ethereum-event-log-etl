import { IBaseModel } from '../IBaseModel';

/**
 *
 *
 * @export
 * @interface IPrice
 */
export interface IPrice {
  ethbtc: string;
  ethbtc_timestamp: string;
  ethusd: string;
  ethusd_timestamp: string;
}

/**
 *
 *
 * @export
 * @interface IEtherPrice
 * @extends {IBaseModel}
 */
export interface IEtherPrice extends IBaseModel {
  result: IPrice;
}
