import { IBaseGethModel } from './IBaseGethModel';
import { IGethTransaction } from './IGethTransaction';

/**
 *
 *
 * @export
 * @interface IBlock
 */
export interface IBlock {
  difficulty: string;
  extraData: string;
  gasLimit: string;
  gasUsed: string;
  hash: string;
  logsBloom: string;
  miner: string;
  mixHash: string;
  nonce: string;
  number: string;
  parentHash: string;
  receiptsRoot: string;
  sha3Uncles: string;
  size: string;
  stateRoot: string;
  timestamp: string;
  totalDifficulty: string;
  transactions: IGethTransaction[];
  transactionsRoot: string;
  uncles: any[];
}

/**
 *
 *
 * @export
 * @interface IGethBlock
 * @extends {IBaseGethModel}
 */
export interface IGethBlock extends IBaseGethModel {
  result: IBlock;
}
