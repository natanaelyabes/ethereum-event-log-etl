import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethBlockNumber
 * @extends {IBaseGethModel}
 */
export interface IGethBlockNumber extends IBaseGethModel {
  result: string;
}
