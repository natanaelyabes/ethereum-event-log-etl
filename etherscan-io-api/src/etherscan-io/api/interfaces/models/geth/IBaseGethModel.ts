/**
 *
 *
 * @export
 * @interface IBaseGethModel
 */
export interface IBaseGethModel {
  jsonrpc: string;
  id: number;
}
