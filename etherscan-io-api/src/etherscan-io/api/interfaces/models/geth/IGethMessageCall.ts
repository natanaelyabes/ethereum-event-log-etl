import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethMessageCall
 * @extends {IBaseGethModel}
 */
export interface IGethMessageCall extends IBaseGethModel {
  result: string;
}
