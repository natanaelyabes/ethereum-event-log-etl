import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface ITransactionReceipt
 */
export interface ITransactionReceipt {
  blockHash: string;
  blockNumber: string;
  contractAddress: string;
  cumulativeGasUsed: string;
  from: string;
  gasUsed: string;
  logs: ILog[] | IData[];
  logsBloom: string;
  root: string;
  to: string;
  transactionHash: string;
  transactionIndex: string;
}

/**
 *
 *
 * @export
 * @interface ILog
 */
export interface ILog {
  address: string;
  topics: string[];
  data: string | IData;
  blockNumber: string;
  transactionHash: string;
  transactionIndex: string;
  blockHash: string;
  logIndex: string;
  removed: boolean;
}

export interface IData {
  name: string;
  events: any[][];
  address: string;
}

/**
 *
 *
 * @export
 * @interface IGethTransactionReceipt
 * @extends {IBaseGethModel}
 */
export interface IGethTransactionReceipt extends IBaseGethModel {
  result: ITransactionReceipt;
}
