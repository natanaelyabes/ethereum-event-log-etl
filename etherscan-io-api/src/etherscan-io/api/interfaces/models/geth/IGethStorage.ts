import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethStorage
 * @extends {IBaseGethModel}
 */
export interface IGethStorage extends IBaseGethModel {
  result: string;
}
