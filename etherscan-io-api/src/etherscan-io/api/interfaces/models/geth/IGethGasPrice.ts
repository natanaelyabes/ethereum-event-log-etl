import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethGasPrice
 * @extends {IBaseGethModel}
 */
export interface IGethGasPrice extends IBaseGethModel {
  result: string;
}
