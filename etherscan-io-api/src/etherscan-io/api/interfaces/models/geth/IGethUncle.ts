import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IUncle
 */
export interface IUncle {
  difficulty: string;
  extraData: string;
  gasLimit: string;
  gasUsed: string;
  hash: string;
  logsBloom: string;
  miner: string;
  mixHash: string;
  nonce: string;
  number: string;
  parentHash: string;
  receiptsRoot: string;
  sha3Uncles: string;
  size: string;
  stateRoot: string;
  timestamp: string;
  totalDifficulty: string;
  transactionsRoot: string;
  uncles: any[];
}

/**
 *
 *
 * @export
 * @interface IGethUncle
 * @extends {IBaseGethModel}
 */
export interface IGethUncle extends IBaseGethModel {
  result: IUncle;
}
