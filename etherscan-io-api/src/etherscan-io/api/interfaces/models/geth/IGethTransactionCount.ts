import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethTransactionCount
 * @extends {IBaseGethModel}
 */
export interface IGethTransactionCount extends IBaseGethModel {
  result: string;
}
