import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethUsedGas
 * @extends {IBaseGethModel}
 */
export interface IGethUsedGas extends IBaseGethModel {
  result: string;
}
