import { IBaseGethModel } from './IBaseGethModel';

/**
 *
 *
 * @export
 * @interface IGethCode
 * @extends {IBaseGethModel}
 */
export interface IGethCode extends IBaseGethModel {
  result: string;
}
