/**
 *
 *
 * @export
 * @interface IBaseModel
 */
export interface IBaseModel {
  status: string;
  message: string;
}
