import { AxiosResponse } from 'axios';
import { IContractSourceCode } from './models/contracts/IContractSourceCodes';
import { IContractABI } from './models/contracts/IContractABI';

/**
 *
 *
 * @export
 * @interface IContractApi
 */
export interface IContractApi {
  getContractABI(address: string): Promise<AxiosResponse<IContractABI>>;
  getContractSourceCode(address: string): Promise<AxiosResponse<IContractSourceCode>>;
}
