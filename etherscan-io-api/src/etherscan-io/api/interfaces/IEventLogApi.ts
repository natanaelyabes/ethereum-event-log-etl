import { AxiosResponse } from 'axios';
import { IEventLogs } from './models/logs/IEventLogs';

/**
 *
 *
 * @export
 * @interface ITopic
 */
export interface ITopic {
  topic0?: string;
  topic1?: string;
  topic2?: string;
  topic3?: string;
  topic0_1_opr?: 'and' | 'or';
  topic0_2_opr?: 'and' | 'or';
  topic0_3_opr?: 'and' | 'or';
  topic1_2_opr?: 'and' | 'or';
  topic1_3_opr?: 'and' | 'or';
  topic2_3_opr?: 'and' | 'or';
}

/**
 *
 *
 * @export
 * @interface IEventLogApi
 */
export interface IEventLogApi {
  getLogs(address: string, fromBlock: number, toBlock: 'latest' | number, topic?: ITopic): Promise<AxiosResponse<IEventLogs>>;
}
