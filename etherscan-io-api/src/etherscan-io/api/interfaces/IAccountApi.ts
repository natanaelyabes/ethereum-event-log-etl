// Libraries
import { AxiosResponse } from 'axios';

// Ether Balance
import { IEtherBalance } from './models/accounts/IEtherBalance';
import { IEtherBalanceMultiAddress } from './models/accounts/IEtherBalanceMultiAddress';

// Blocks
import { IMinedBlocks } from './models/accounts/IMinedBlocks';

// Transactions
import { IERC20TokenTransferEventTransactions } from './models/accounts/IERC20TokenTransferEventTransactions';
import { INormalTransactions } from './models/accounts/INormalTransactions';
import { IInternalTransactions } from './models/accounts/IInternalTransactions';

/**
 *
 *
 * @export
 * @interface ILimit
 */
export interface ILimit {
  page: number;
  offset: number;
}

/**
 *
 *
 * @export
 * @interface IAccountApi
 */
export interface IAccountApi {
  getEtherBalance(address: string | string[], tag: string): Promise<AxiosResponse<IEtherBalance | IEtherBalanceMultiAddress>>;
  getNormalTransactionsByAddress(address: string, startBlock: number, endBlock: number, sort: 'asc' | 'desc', limit?: ILimit): Promise<AxiosResponse<INormalTransactions>>;
  getInternalTransactionsByAddress(address: string, startBlock: number, endBlock: number, sort: 'asc' | 'desc', limit?: ILimit): Promise<AxiosResponse<IInternalTransactions>>;
  getInternalTransactionsByHash(txHash: string): Promise<AxiosResponse<IInternalTransactions>>;
  getERC20TokenTransferEventTransactionByAddress(address: string, startBlock: number, endBlock: number, sort: 'asc' | 'desc', limit?: ILimit): Promise<AxiosResponse<IERC20TokenTransferEventTransactions>>;
  getMinedBlocks(address: string, blockType: 'blocks' | 'uncles', limit?: ILimit): Promise<AxiosResponse<IMinedBlocks>>;
}
