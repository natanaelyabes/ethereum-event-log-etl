import { AxiosResponse } from 'axios';
import { IBlockReward } from './models/blocks/IBlockReward';

/**
 *
 *
 * @export
 * @interface IBlockApi
 */
export interface IBlockApi {
  getBlockReward(blockNo: number): Promise<AxiosResponse<IBlockReward>>;
}
