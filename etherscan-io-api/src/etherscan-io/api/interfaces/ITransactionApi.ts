import { AxiosResponse } from 'axios';
import { ITransactionReceiptStatus } from './models/transactions/ITransactionReceiptStatus';
import { IContractExecutionStatus } from './models/transactions/IContractExecutionStatus';

/**
 *
 *
 * @export
 * @interface ITransactionApi
 */
export interface ITransactionApi {
  getContractExecutionStatus(txHash: string): Promise<AxiosResponse<IContractExecutionStatus>>;
  getTransactionReceiptStatus(txHash: string): Promise<AxiosResponse<ITransactionReceiptStatus>>;
}
