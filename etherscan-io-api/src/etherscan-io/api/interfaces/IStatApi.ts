import { AxiosResponse } from 'axios';
import { IEthereumNodes } from './models/stats/IEthereumNodes';
import { IEtherPrice } from './models/stats/IEtherPrice';
import { ISimpleResult } from './models/ISimpleResult';

/**
 *
 *
 * @export
 * @interface IStatApi
 */
export interface IStatApi {
  getTotalSupplyOfEther(): Promise<AxiosResponse<ISimpleResult>>;
  getEtherLastPrice(): Promise<AxiosResponse<IEtherPrice>>;
  getEthereumNodeSize(startDate: string, endDate: string, clientType: 'geth' | 'parity', syncMode: 'default' | 'archive', sort: 'asc' | 'desc'): Promise<AxiosResponse<IEthereumNodes>>;
}
