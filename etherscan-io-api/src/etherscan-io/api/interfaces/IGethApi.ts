import { IGethTransactionReceipt } from './models/geth/IGethTransactionReceipt';
import { AxiosResponse } from 'axios';
import { IGethUsedGas } from './models/geth/IGethUsedGas';
import { IGethGasPrice } from './models/geth/IGethGasPrice';
import { IGethStorage } from './models/geth/IGethStorage';
import { IGethCode } from './models/geth/IGethCode';
import { IGethMessageCall } from './models/geth/IGethMessageCall';
import { IGethTransactionCount } from './models/geth/IGethTransactionCount';
import { IGethUncle } from './models/geth/IGethUncle';
import { IGethBlock } from './models/geth/IGethBlock';
import { IGethBlockNumber } from './models/geth/IGethBlockNumber';
import { IGethTransaction, IGethTransactions } from './models/geth/IGethTransaction';

/**
 *
 *
 * @export
 * @interface IGethApi
 */
export interface IGethApi {
  getEthBlockNumber(): Promise<AxiosResponse<IGethBlockNumber>>;
  getEthBlockByNumber(tag: string, boolean: boolean): Promise<AxiosResponse<IGethBlock>>;
  getEthUncleByBlockNumberAndIndex(tag: string, index: string): Promise<AxiosResponse<IGethUncle>>;
  getEthBlockTransactionCountByNumber(tag: string): Promise<AxiosResponse<IGethTransactionCount>>;
  getEthTransactionByHash(txHash: string): Promise<AxiosResponse<IGethTransactions>>;
  getEthTransactionByBlockNumberAndIndex(tag: string, index: string): Promise<AxiosResponse<IGethTransactions>>;
  getEthTransactionCount(address: string, tag: string): Promise<AxiosResponse<IGethTransactionCount>>;
  getEthTransactionReceipt(txHash: string): Promise<AxiosResponse<IGethTransactionReceipt>>;
  getEthCall(to: string, data: string, tag: string): Promise<AxiosResponse<IGethMessageCall>>;
  getEthCode(address: string, tag: string): Promise<AxiosResponse<IGethCode>>;
  getEthStorageAt(address: string, position: string, tag: string): Promise<AxiosResponse<IGethStorage>>;
  getEthGasPrice(): Promise<AxiosResponse<IGethGasPrice>>;
  estimateEthGas(to: string, value: string, gasPrice: string, gas: string): Promise<AxiosResponse<IGethUsedGas>>;
}
