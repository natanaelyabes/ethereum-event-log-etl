import { MODULE_NAME, ACTIONS } from './enums/EVENT_LOGS';
import { IEventLogApi, ITopic } from './interfaces/IEventLogApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { IEventLogs } from './interfaces/models/logs/IEventLogs';

/**
 *
 *
 * @export
 * @class EventLogApi
 * @extends {EtherscanApi}
 * @implements {IEventLogApi}
 */
export class EventLogApi extends EtherscanApi implements IEventLogApi {

  /**
   * Creates an instance of EventLogApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof EventLogApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @param {string} address
   * @param {number} fromBlock
   * @param {(number | 'latest')} toBlock
   * @param {(ITopic | undefined)} [topic]
   * @returns {Promise<AxiosResponse<IEventLogs>>}
   * @memberof EventLogApi
   */
  public async getLogs(address: string, fromBlock: number, toBlock: number | 'latest', topic?: ITopic | undefined): Promise<AxiosResponse<IEventLogs>> {

    // Set action
    this.action = ACTIONS.getlogs;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pFromBlock = `fromBlock=${fromBlock}`;
    const pToBlock = `toBlock=${toBlock}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;

    // Construct parameter
    let p = [pModule, pAction, pAddress, pFromBlock, pToBlock].join('&');

    // Construct topic parameter
    let pTopic;

    if (topic) {
      // Construct topic
      const topic0 = `topic0=${topic!.topic0}`;
      const topic1 = `topic1=${topic!.topic1}`;
      const topic2 = `topic2=${topic!.topic2}`;
      const topic3 = `topic3=${topic!.topic3}`;

      // Construct topic operator
      const topic0_1_opr = `topic0_1_opr=${topic!.topic0_1_opr}`;
      const topic0_2_opr = `topic0_2_opr=${topic!.topic0_2_opr}`;
      const topic0_3_opr = `topic0_3_opr=${topic!.topic0_3_opr}`;
      const topic1_2_opr = `topic1_2_opr=${topic!.topic1_2_opr}`;
      const topic1_3_opr = `topic1_3_opr=${topic!.topic1_3_opr}`;
      const topic2_3_opr = `topic2_3_opr=${topic!.topic2_3_opr}`;

      if (topic.topic0 && !topic.topic1 && !topic.topic2 && !topic.topic3) {
        pTopic = `${topic0}`;
      }

      if (!topic.topic0 && topic.topic1 && !topic.topic2 && !topic.topic3) {
        pTopic = `${topic1}`;
      }

      if (!topic.topic0 && !topic.topic1 && topic.topic2 && !topic.topic3) {
        pTopic = `${topic2}`;
      }

      if (!topic.topic0 && !topic.topic1 && !topic.topic2 && topic.topic3) {
        pTopic = `${topic3}`;
      }

      if (topic.topic0 && topic.topic1 && !topic.topic2 && !topic.topic3) {
        pTopic = `${topic0}&${topic0_1_opr}&${topic1}`;
      }

      if (topic.topic0 && !topic.topic1 && topic.topic2 && !topic.topic3) {
        pTopic = `${topic0}&${topic0_2_opr}&${topic2}`;
      }

      if (topic.topic0 && !topic.topic1 && !topic.topic2 && topic.topic3) {
        pTopic = `${topic0}&${topic0_3_opr}&${topic3}`;
      }

      if (!topic.topic0 && topic.topic1 && topic.topic2 && !topic.topic3) {
        pTopic = `${topic1}&${topic1_2_opr}&${topic2}`;
      }

      if (!topic.topic0 && topic.topic1 && !topic.topic2 && topic.topic3) {
        pTopic = `${topic1}&${topic1_3_opr}&${topic3}`;
      }

      if (!topic.topic0 && !topic.topic1 && topic.topic2 && topic.topic3) {
        pTopic = `${topic2}&${topic2_3_opr}&${topic3}`;
      }

      p = [pModule, pAction, pAddress, pFromBlock, pToBlock, pTopic].join('&');
    }

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch eventLog
    const eventLog: AxiosResponse<IEventLogs> = await this.fetch(url);

    // Return eventLog
    return eventLog;
  }
}
