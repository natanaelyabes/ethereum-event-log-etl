import { IGethTransactionReceipt } from './interfaces/models/geth/IGethTransactionReceipt';
import { MODULE_NAME, ACTIONS } from './enums/GETH';
import { IGethApi } from './interfaces/IGethApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { IGethBlockNumber } from './interfaces/models/geth/IGethBlockNumber';
import { IGethBlock } from './interfaces/models/geth/IGethBlock';
import { IGethUncle } from './interfaces/models/geth/IGethUncle';
import { IGethTransactionCount } from './interfaces/models/geth/IGethTransactionCount';
import { IGethTransaction, IGethTransactions } from './interfaces/models/geth/IGethTransaction';
import { IGethMessageCall } from './interfaces/models/geth/IGethMessageCall';
import { IGethCode } from './interfaces/models/geth/IGethCode';
import { IGethStorage } from './interfaces/models/geth/IGethStorage';
import { IGethGasPrice } from './interfaces/models/geth/IGethGasPrice';
import { IGethUsedGas } from './interfaces/models/geth/IGethUsedGas';

/**
 *
 *
 * @export
 * @class GethApi
 * @extends {EtherscanApi}
 * @implements {IGethApi}
 */
export class GethApi extends EtherscanApi implements IGethApi {

  /**
   * Creates an instance of GethApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof GethApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  public async getEthBlockNumber(): Promise<AxiosResponse<IGethBlockNumber>> {

    // Set action
    this.action = ACTIONS.eth_blockNumber;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethBlockNumber
    const ethBlockNumber: AxiosResponse<IGethBlockNumber> = await this.fetch(url);

    // Return ethBlockNumber
    return ethBlockNumber;
  }

  public async getEthBlockByNumber(tag: string, boolean: boolean): Promise<AxiosResponse<IGethBlock>> {

    // Set action
    this.action = ACTIONS.eth_getBlockByNumber;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTag = `tag=${tag}`;
    const pBoolean = `boolean=${boolean}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTag, pBoolean].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethBlockByNumber
    const ethBlockByNumber: AxiosResponse<IGethBlock> = await this.fetch(url);

    // Return ethBlockByNumber
    return ethBlockByNumber;
  }

  public async getEthUncleByBlockNumberAndIndex(tag: string, index: string): Promise<AxiosResponse<IGethUncle>> {

    // Set action
    this.action = ACTIONS.eth_getUncleByNumberAndIndex;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTag = `tag=${tag}`;
    const pIndex = `index=${index}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTag, pIndex].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethUncleByBlockNumberAndIndex
    const ethUncleByBlockNumberAndIndex: AxiosResponse<IGethUncle> = await this.fetch(url);

    // Return ethUncleByBlockNumberAndIndex
    return ethUncleByBlockNumberAndIndex;
  }

  public async getEthBlockTransactionCountByNumber(tag: string): Promise<AxiosResponse<IGethTransactionCount>> {

    // Set action
    this.action = ACTIONS.eth_getBlockTransactionCountByNumber;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethBlockTransactionCountByNumber
    const ethBlockTransactionCountByNumber: AxiosResponse<IGethTransactionCount> = await this.fetch(url);

    // Return ethBlockTransactionCountByNumber
    return ethBlockTransactionCountByNumber;
  }

  public async getEthTransactionByHash(txHash: string): Promise<AxiosResponse<IGethTransactions>> {

    // Set action
    this.action = ACTIONS.eth_getTransactionByHash;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTxHash = `txhash=${txHash}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTxHash].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethTransactionByHash
    const ethTransactionByHash: AxiosResponse<IGethTransactions> = await this.fetch(url);

    // Return ethTransactionByHash
    return ethTransactionByHash;
  }

  public async getEthTransactionByBlockNumberAndIndex(tag: string, index: string): Promise<AxiosResponse<IGethTransactions>> {

    // Set action
    this.action = ACTIONS.eth_getTransactionByBlockNumberAndIndex;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTag = `tag=${tag}`;
    const pIndex = `index=${index}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTag, pIndex].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethTransactionByBlockNumberAndIndex
    const ethTransactionByBlockNumberAndIndex: AxiosResponse<IGethTransactions> = await this.fetch(url);

    // Return ethTransactionByBlockNumberAndIndex
    return ethTransactionByBlockNumberAndIndex;
  }

  public async getEthTransactionCount(address: string, tag: string): Promise<AxiosResponse<IGethTransactionCount>> {

    // Set action
    this.action = ACTIONS.eth_getTransactionCount;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddress, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethTransactionCount
    const ethTransactionCount: AxiosResponse<IGethTransactionCount> = await this.fetch(url);

    // Return ethTransactionCount
    return ethTransactionCount;
  }

  public async getEthTransactionReceipt(txHash: string): Promise<AxiosResponse<IGethTransactionReceipt>> {

    // Set action
    this.action = ACTIONS.eth_getTransactionReceipt;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTxHash = `txhash=${txHash}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTxHash].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethTransactionReceipt
    const ethTransactionReceipt: AxiosResponse<IGethTransactionReceipt> = await this.fetch(url);

    // Return ethTransactionReceipt
    return ethTransactionReceipt;
  }

  public async getEthCall(to: string, data: string, tag: string): Promise<AxiosResponse<IGethMessageCall>> {

    // Set action
    this.action = ACTIONS.eth_call;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTo = `to=${to}`;
    const pData = `data=${data}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTo, pData, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethMessageCall
    const ethMessageCall: AxiosResponse<IGethMessageCall> = await this.fetch(url);

    // Return ethMessageCall
    return ethMessageCall;
  }

  public async getEthCode(address: string, tag: string): Promise<AxiosResponse<IGethCode>> {

    // Set action
    this.action = ACTIONS.eth_getCode;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddress, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethCode
    const ethCode: AxiosResponse<IGethCode> = await this.fetch(url);

    // Return ethCode
    return ethCode;
  }

  public async getEthStorageAt(address: string, position: string, tag: string): Promise<AxiosResponse<IGethStorage>> {

    // Set action
    this.action = ACTIONS.eth_getCode;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pPosition = `position=${position}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddress, pPosition, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethStorageAt
    const ethStorageAt: AxiosResponse<IGethStorage> = await this.fetch(url);

    // Return ethStorageAt
    return ethStorageAt;
  }

  public async getEthGasPrice(): Promise<AxiosResponse<IGethGasPrice>> {

    // Set action
    this.action = ACTIONS.eth_gasPrice;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethGasPrice
    const ethGasPrice: AxiosResponse<IGethGasPrice> = await this.fetch(url);

    // Return ethGasPrice
    return ethGasPrice;
  }

  public async estimateEthGas(to: string, value: string, gasPrice: string, gas: string): Promise<AxiosResponse<IGethUsedGas>> {

    // Set action
    this.action = ACTIONS.eth_estimateGas;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTo = `to=${to}`;
    const pValue = `value=${value}`;
    const pGasPrice = `gasPrice=${gasPrice}`;
    const pGas = `gas=${gas}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTo, pValue, pGasPrice, pGas].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethGasPrice
    const ethGasPrice: AxiosResponse<IGethGasPrice> = await this.fetch(url);

    // Return ethGasPrice
    return ethGasPrice;
  }
}
