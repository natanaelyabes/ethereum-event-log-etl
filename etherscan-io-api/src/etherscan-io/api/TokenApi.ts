import { MODULE_NAME, ACTIONS } from './enums/TOKENS';
import { ITokenApi } from './interfaces/ITokenApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { ISimpleResult } from './interfaces/models/ISimpleResult';

/**
 *
 *
 * @export
 * @class TokenApi
 * @extends {EtherscanApi}
 * @implements {ITokenApi}
 */
export class TokenApi extends EtherscanApi implements ITokenApi {

  /**
   * Creates an instance of TokenApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof TokenApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @param {string} contractAddress
   * @returns {Promise<AxiosResponse<ISimpleResult>>}
   * @memberof TokenApi
   */
  public async getERC20TokenTotalSupplyByContractAddress(contractAddress: string): Promise<AxiosResponse<ISimpleResult>> {

    // Set action
    this.action = ACTIONS.tokensupply;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pContractAddress = `contractaddress=${contractAddress}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pContractAddress].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch tokenSupply
    const tokenSupply: AxiosResponse<ISimpleResult> = await this.fetch(url);

    // Return tokenSupply
    return tokenSupply;
  }

  /**
   *
   *
   * @param {string} contractAddress
   * @param {string} address
   * @param {string} tag
   * @returns {Promise<AxiosResponse<ISimpleResult>>}
   * @memberof TokenApi
   */
  public async getERC20TokenAccountBalanceForTokenContractAddress(contractAddress: string, address: string, tag: string): Promise<AxiosResponse<ISimpleResult>> {

    this.module = 'account';

    // Set action
    this.action = ACTIONS.tokenbalance;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pAddress = `address=${address}`;
    const pContractAddress = `contractaddress=${contractAddress}`;
    const pTag = `tag=${tag}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pAddress, pContractAddress, pTag].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch tokenBalance
    const tokenBalance: AxiosResponse<ISimpleResult> = await this.fetch(url);

    // Return tokenBalance
    return tokenBalance;
  }
}
