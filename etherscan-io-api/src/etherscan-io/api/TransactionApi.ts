import { MODULE_NAME, ACTIONS } from './enums/TRANSACTIONS';
import { ITransactionApi } from './interfaces/ITransactionApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { IContractExecutionStatus } from './interfaces/models/transactions/IContractExecutionStatus';
import { ITransactionReceiptStatus } from './interfaces/models/transactions/ITransactionReceiptStatus';

/**
 *
 *
 * @export
 * @class TransactionApi
 * @extends {EtherscanApi}
 * @implements {ITransactionApi}
 */
export class TransactionApi extends EtherscanApi implements ITransactionApi {

  /**
   * Creates an instance of TransactionApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof TransactionApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @param {string} txHash
   * @returns {Promise<AxiosResponse<IContractExecutionStatus>>}
   * @memberof TransactionApi
   */
  public async getContractExecutionStatus(txHash: string): Promise<AxiosResponse<IContractExecutionStatus>> {

    // Set action
    this.action = ACTIONS.getstatus;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTxHash = `txhash=${txHash}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTxHash].join('&');

    // Construct url
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch contractExecutionStatus
    const contractExecutionStatus: AxiosResponse<IContractExecutionStatus> = await this.fetch(url);

    // Return contractExecutionStatus
    return contractExecutionStatus;
  }

  /**
   *
   *
   * @param {string} txHash
   * @returns {Promise<AxiosResponse<ITransactionReceiptStatus>>}
   * @memberof TransactionApi
   */
  public async getTransactionReceiptStatus(txHash: string): Promise<AxiosResponse<ITransactionReceiptStatus>> {

    // Set action
    this.action = ACTIONS.gettxreceiptstatus;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pTxHash = `txhash=${txHash}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pTxHash].join('&');

    // Construct url
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch transactionReceiptStatus
    const transactionReceiptStatus: AxiosResponse<ITransactionReceiptStatus> = await this.fetch(url);

    // Return transactionReceiptStatus
    return transactionReceiptStatus;
  }
}
