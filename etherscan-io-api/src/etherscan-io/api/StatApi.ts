import { MODULE_NAME, ACTIONS } from './enums/STATS';
import { IStatApi } from './interfaces/IStatApi';
import { EtherscanApi } from './base/EtherscanApi';
import { AxiosResponse } from 'axios';
import { ISimpleResult } from './interfaces/models/ISimpleResult';
import { IEtherPrice } from './interfaces/models/stats/IEtherPrice';
import { IEthereumNodes } from './interfaces/models/stats/IEthereumNodes';

/**
 *
 *
 * @export
 * @class StatApi
 * @extends {EtherscanApi}
 * @implements {IStatApi}
 */
export class StatApi extends EtherscanApi implements IStatApi {

  /**
   * Creates an instance of StatApi.
   * @param {string} BASE_URL
   * @param {string} API_TOKEN
   * @memberof StatApi
   */
  constructor(BASE_URL: string, API_TOKEN: string) {
    super(BASE_URL, API_TOKEN);
    this.module = MODULE_NAME;
  }

  /**
   *
   *
   * @returns {Promise<AxiosResponse<ISimpleResult>>}
   * @memberof StatApi
   */
  public async getTotalSupplyOfEther(): Promise<AxiosResponse<ISimpleResult>> {

    // Set action
    this.action = ACTIONS.ethsupply;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch totalSupplyOfEther
    const totalSupplyOfEther: AxiosResponse<ISimpleResult> = await this.fetch(url);

    // Return totalSupplyOfEther
    return totalSupplyOfEther;
  }

  /**
   *
   *
   * @returns {Promise<AxiosResponse<IEtherPrice>>}
   * @memberof StatApi
   */
  public async getEtherLastPrice(): Promise<AxiosResponse<IEtherPrice>> {

    // Set action
    this.action = ACTIONS.ethprice;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch etherLastPrice
    const etherLastPrice: AxiosResponse<IEtherPrice> = await this.fetch(url);

    // Return etherLastPrice
    return etherLastPrice;
  }

  /**
   *
   *
   * @param {string} startDate
   * @param {string} endDate
   * @param {('geth' | 'parity')} clientType
   * @param {('default' | 'archive')} syncMode
   * @param {('asc' | 'desc')} sort
   * @returns {Promise<AxiosResponse<IEthereumNodes>>}
   * @memberof StatApi
   */
  public async getEthereumNodeSize(startDate: string, endDate: string, clientType: 'geth' | 'parity', syncMode: 'default' | 'archive', sort: 'asc' | 'desc'): Promise<AxiosResponse<IEthereumNodes>> {

    // Set action
    this.action = ACTIONS.chainsize;

    // Construct parameter
    const pModule = `module=${this.module}`;
    const pAction = `action=${this.action}`;
    const pStartDate = `startdate=${startDate}`;
    const pEndDate = `enddate=${endDate}`;
    const pClientType = `clienttype=${clientType}`;
    const pSyncMode = `syncmode=${syncMode}`;
    const pSort = `sort=${sort}`;
    const pApiKey = `apikey=${this.API_TOKEN}`;
    const p = [pModule, pAction, pStartDate, pEndDate, pClientType, pSyncMode, pSort].join('&');

    // Construct URL
    const url = `${this.BASE_URL}?${p}&${pApiKey}`;

    // Fetch ethereumNodeSize
    const ethereumNodeSize: AxiosResponse<IEthereumNodes> = await this.fetch(url);

    // Return ethereumNodeSize
    return ethereumNodeSize;
  }
}
