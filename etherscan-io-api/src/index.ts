// Libraries
import * as FileSystem from '@netanyahuyasser/file-system-service';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import fs from 'fs';
import morgan from 'morgan';
import path from 'path';
import favicon from 'serve-favicon';

// Etherscan IO API
import Main from './Main';
// Main.run();

dotenv.config();

const server = express();

const corsOptions: cors.CorsOptions = {
  optionsSuccessStatus: 200,
  origin: '*',
};

server.use(morgan('dev'));
server.use(cors(corsOptions));
server.use(bodyParser.json());
server.use(favicon(path.join(process.cwd(), 'public', 'images', 'icon', 'favicon.ico')));

server.get('/', (req: express.Request, res: express.Response) => {
  res.send(/*html*/`
    <h1>${process.env.APP_NAME} works!<h1>
  `);
});

const port: number | string = process.env.APP_PORT || process.env.port as number | string;

server.listen(port, () => {
  process.stdout.write(`Server started at ${port}`);
});
