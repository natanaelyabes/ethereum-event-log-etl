package kr.ac.pusan.bsclab.generator.xeslite;

import java.io.Serializable;

import org.deckfour.xes.model.XAttributeMap;

public interface XAttributeMapImpl extends XAttributeMap, Serializable {

}
