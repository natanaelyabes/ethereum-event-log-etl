package kr.ac.pusan.bsclab.generator.xeslite;

import java.io.Serializable;

import kr.ac.pusan.bsclab.generator.interfaces.IXLog;

public class XLogImpl implements IXLog, Serializable {
  
  /**
   * 
   */
  private static final long serialVersionUID = -2590128356718385453L;
  
  public int caseId = 0;
  public String eventId = "";
  public String timestamp = "";
  public String blockNumber = "";
  public String activity = "";
  
  public XLogImpl() {
    super();
  }

  public int getCaseId() {
    return caseId;
  }

  public void setCaseId(int caseId) {
    this.caseId = caseId;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getBlockNumber() {
    return blockNumber;
  }

  public void setBlockNumber(String blockNumber) {
    this.blockNumber = blockNumber;
  }

  public String getActivity() {
    return activity;
  }

  public void setActivity(String activity) {
    this.activity = activity;
  }
  
  
}
