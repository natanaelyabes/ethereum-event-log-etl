package kr.ac.pusan.bsclab.generator.interfaces;

public interface IXLog {
  public int caseId = 0;
  public String eventId = "";
  public String timestamp = "";
//  public String blockNumber = "";
  public String activity = "";
}