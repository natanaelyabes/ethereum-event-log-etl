package kr.ac.pusan.bsclab.generator.spark;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.sql.SparkSession;
import org.deckfour.xes.classification.XEventAttributeClassifier;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XOrganizationalExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.out.XesXmlSerializer;

import kr.ac.pusan.bsclab.generator.xeslite.XFactoryLiteSerializable;
import kr.ac.pusan.bsclab.generator.xeslite.XLogImpl;
import scala.Tuple2;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class XESGenerator {
  public static void main(String args[]) {
    run();
  }

  public static void run() {
    // Configure spark and create a spark context.
    SparkSession spark = SparkSession.builder().appName("Spark XES Generator").master("local[4]").getOrCreate();
    JavaSparkContext sc = new JavaSparkContext(spark.sparkContext());

    // Put the event log into RDD.
    JavaRDD<String> genesisCSV = processGenesisCSV(sc);
    
    // Preprocess the event log.
    JavaRDD<XLogImpl> genesisEventLog = mapToXLogImpl(genesisCSV);
    JavaPairRDD<Integer, Iterable<XLogImpl>> genesisEventMap = mapToPairRDD(genesisEventLog);
    JavaPairRDD<Integer, Iterable<XLogImpl>> genesisEventMapSorted = genesisEventMap.sortByKey();
    
    // Save to XES
    saveToXES(genesisEventMapSorted, "out/genesis.xes");

    // Put the event log into RDD.
    JavaRDD<String> everydayCSV = processEverydayCSV(sc);
    
    // Preprocess the event log.
    JavaRDD<XLogImpl> everydayEventLog = mapToXLogImpl(everydayCSV);
    JavaPairRDD<Integer, Iterable<XLogImpl>> everydayEventMap = mapToPairRDD(everydayEventLog);
    JavaPairRDD<Integer, Iterable<XLogImpl>> everydayEventMapSorted = everydayEventMap.sortByKey();
    
    saveToXES(everydayEventMapSorted, "out/everyday.xes");
    
    // Print the size of each event log
    System.out.println("Genesis event log size: " + genesisCSV.count());
    System.out.println("Everyday event log size: " + everydayCSV.count());
    
    // Close and stop the spark context.
    sc.close(); sc.stop();
  }

  private static void saveToXES(JavaPairRDD<Integer, Iterable<XLogImpl>> eventMap, String path) {
    XFactory factory = new XFactoryLiteSerializable();
    XFactoryRegistry.instance().setCurrentDefault(factory);
    
    // Create XLog
    XLog log = factory.createLog();
    log.getExtensions().add(XTimeExtension.instance());
    log.getExtensions().add(XLifecycleExtension.instance());
    log.getExtensions().add(XConceptExtension.instance());
    log.getExtensions().add(XOrganizationalExtension.instance());
    log.getClassifiers().add(new XEventAttributeClassifier("Event Name", XConceptExtension.KEY_NAME));
    log.getClassifiers().add(new XEventAttributeClassifier("(Event Name AND Lifecyle transition)", XConceptExtension.KEY_NAME, XLifecycleExtension.KEY_TRANSITION));
    
    // Log Attributes
    XAttribute author = createAttributeLiteral(factory, "Author", "Natanael Yabes Wirawan <yabes.wirawan@pusan.ac.kr>", XConceptExtension.instance());
    XAttribute org = createAttributeLiteral(factory, "Organization", "Pusan National University", XConceptExtension.instance());
    
    // Log Attribute Map
    XAttributeMap map = factory.createAttributeMap();
    map.put(author.getKey(), author);
    map.put(org.getKey(), org);
    
    // Set attribute map
    log.setAttributes(map);
    
    // Add all traces to log
    log.addAll(getTraces(eventMap, factory));
    
    // Parse log object to XES file
    XesXmlSerializer serializer = new XesXmlSerializer();
    
    // Serialize to XES
    try {
      serializer.serialize(log, new FileOutputStream(path));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static Collection<? extends XTrace> getTraces(JavaPairRDD<Integer, Iterable<XLogImpl>> eventMap, XFactory factory) {
    return (Collection<? extends XTrace>) eventMap.map(new Function<Tuple2<Integer, Iterable<XLogImpl>>, XTrace>() {

      /**
       * 
       */
      private static final long serialVersionUID = 1L;

      @Override
      public XTrace call(Tuple2<Integer, Iterable<XLogImpl>> t) throws Exception {
        // Trace
        XTrace trace = factory.createTrace();
        
        // Trace Attributes
        XAttribute caseId = createAttributeLiteral(factory, XConceptExtension.KEY_NAME, t._1().toString(), XConceptExtension.instance());
        
        // Attribute Map
        XAttributeMap traceMap = factory.createAttributeMap();
        traceMap.put(caseId.getKey(), caseId);
        
        // Set Attribute Map 
        trace.setAttributes(traceMap);
        
        for (Iterator<XLogImpl> iterator = t._2().iterator(); iterator.hasNext(); ) {
          // Event
          XEvent event = factory.createEvent();
          
          // Event from iterator
          XLogImpl e = (XLogImpl) iterator.next();
          
          // Attribute
          XAttribute eventId = createAttributeLiteral(factory, "event", e.eventId, XConceptExtension.instance());
          XAttribute activity = createAttributeLiteral(factory, XConceptExtension.KEY_NAME, e.activity, XConceptExtension.instance());
          XAttribute timestamp = factory.createAttributeTimestamp(XTimeExtension.KEY_TIMESTAMP, Long.parseLong(e.timestamp), XTimeExtension.instance());
          XAttribute lifecyle = createAttributeLiteral(factory, XLifecycleExtension.KEY_TRANSITION, "complete", XLifecycleExtension.instance());
          
          // Attribute Map
          XAttributeMap eventMap = factory.createAttributeMap();
          eventMap.put(eventId.getKey(), eventId);
          eventMap.put(activity.getKey(), activity);
          eventMap.put(timestamp.getKey(), timestamp);
          eventMap.put(lifecyle.getKey(), lifecyle);
          
          // Set event attribute
          event.setAttributes(eventMap);
          
          // Add event to trace
          trace.add(event);
        }
        return trace;
      }
    }).collect();
  }

  private static XAttribute createAttributeLiteral(XFactory factory, String k, String v, XExtension e) {
    XAttribute attr = factory.createAttributeLiteral(k, v, e);
    return attr;
  }

  private static JavaPairRDD<Integer, Iterable<XLogImpl>> mapToPairRDD(JavaRDD<XLogImpl> log) {
    return log.groupBy(new Function<XLogImpl, Integer>() {

      /**
       * 
       */
      private static final long serialVersionUID = 7812484225170509787L;

      @Override
      public Integer call(XLogImpl event) throws Exception {
        return event.caseId;
      }
    });
  }

  private static JavaRDD<XLogImpl> mapToXLogImpl(JavaRDD<String> csv) {
    return csv.map(new Function<String, XLogImpl>() {

      /**
       * 
       */
      private static final long serialVersionUID = -2371426435618065962L;

      @Override
      public XLogImpl call(String row) throws Exception {
        String[] values  = row.split(",");
        XLogImpl event = new XLogImpl();

        event.caseId = Integer.parseInt(values[0]);
        event.eventId = values[1];
        event.activity = values[2];
        event.timestamp = values[3];

        return event;
      }
    });
  }
  
  private static JavaRDD<String> processGenesisCSV(JavaSparkContext sc) {
    // Retrieve genesisCSV event log and store it as RDD.
    JavaRDD<String> genesisCSV = sc.textFile("hdfs://172.18.0.8:9000/json/ethereum/cryptokitties/genesis/complete.csv");

    // Skip the first row
    genesisCSV = genesisCSV.mapPartitionsWithIndex(skipHeader(), true);
    
    return genesisCSV;
  }

  private static JavaRDD<String> processEverydayCSV(JavaSparkContext sc) {
    // Retrieve genesisCSV event log and store it as RDD.
    JavaRDD<String> everydayCSV = sc.textFile("hdfs://172.18.0.8:9000/json/ethereum/cryptokitties/everyday/complete.csv");
    
    // Skip the first row.
    everydayCSV = everydayCSV.mapPartitionsWithIndex(skipHeader(), true);
    return everydayCSV;
  }
  
  private static Function2<Integer, Iterator<String>, Iterator<String>> skipHeader() {
    return new Function2<Integer, Iterator<String>, Iterator<String>>() {

      private static final long serialVersionUID = 1L;

      @Override
      public Iterator<String> call(Integer i, Iterator<String> it) throws Exception {
        if (i == 0 && it.hasNext()) {
          it.next();
          return it;
        } else {
          return it;          
        }
      }
    };
  }
}
