package kr.ac.pusan.bsclab.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.ac.pusan.bsclab.generator.spark.XESGenerator;

@RestController
@SpringBootApplication
public class DemoApplication {

	@GetMapping(value = "/hello")
	public String getMethodName() {

		XESGenerator.run();
		return "Hello, test";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
