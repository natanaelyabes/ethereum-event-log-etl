package kr.ac.pusan.bsclab.generator.xeslite.example;

// Java Core API
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

// OpenXES
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.classification.XEventAttributeClassifier;
import org.deckfour.xes.extension.std.XIdentityExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.out.XesXmlSerializer;

// XESLite
import org.xeslite.lite.factory.XFactoryLiteImpl;
import org.xeslite.parser.XesLiteXmlParser;

public class ExampleEventLog {
	public static void main(String args[]) throws FileNotFoundException, IOException, Exception {
		createLog();
	}

	public static void readLog() throws FileNotFoundException, Exception {
		XFactory factory = new XFactoryLiteImpl();
		XFactoryRegistry.instance().setCurrentDefault(factory);

		XesLiteXmlParser parser = new XesLiteXmlParser(factory, true);
		List<XLog> logs = parser.parse(new FileInputStream("out/test.xes"));
		System.out.println(logs.get(0).getExtensions());
	}

	public static void createLog() throws FileNotFoundException, IOException {
		XFactory factory = new XFactoryLiteImpl();
		XFactoryRegistry.instance().setCurrentDefault(factory);
		XLog log = factory.createLog();

		// Log extensions, classifiers, and globals
		log.getExtensions().add(XConceptExtension.instance());
		log.getClassifiers().add(new XEventAttributeClassifier("Group", "Group"));
		log.getGlobalEventAttributes().add(new XAttributeLiteralImpl("name", "test"));

		// Create XAttribute for log
		XAttribute logAttr = factory.createAttributeLiteral("name", "Natanael Yabes", XIdentityExtension.instance());
		XAttributeMap logMap = factory.createAttributeMap();
		logMap.put(logAttr.getKey(), logAttr);
		log.setAttributes(logMap);
		XTrace trace = factory.createTrace();
		XEvent event = factory.createEvent();

		// Create XAttribute for event
		XAttribute attr = factory.createAttributeLiteral(XConceptExtension.KEY_NAME, "Test",
				XConceptExtension.instance());
		XAttributeMap map = factory.createAttributeMap();
		map.put(attr.getKey(), attr);

		// Set attribute for event
		event.setAttributes(map);

		// Trace
		trace.add(event);
		log.add(trace);

		XesXmlSerializer serializer = new XesXmlSerializer();

		// Serialize the XLog object to XES file
		serializer.serialize(log, new FileOutputStream("out/test.xes"));
	}
}
